package org.iartisan.wechatsdk

import org.slf4j.LoggerFactory

/**
  * log
  *
  * @author King  2019/7/4
  */
class LogAspect {

   val log = LoggerFactory.getLogger("iartisan-wechatsdk")

   protected val LOG_REQ = "\n===>"

   protected val LOG_RES = "\n<==="

}
