package org.iartisan.wechatsdk.miniprogram

import org.iartisan.runtime.env.EnvContextConfig
import org.iartisan.runtime.utils.StringUtils
import org.iartisan.wechatsdk.JAccessToken
import org.iartisan.wechatsdk.common.{Constant, GlobalCache}

/**
  * 小程序token
  *
  * @author King  2019-8-20
  */
object MiniappAccessToken {
  def getAccessToken: String = {
    val appid = EnvContextConfig.get(Constant.wechat_miniapp_appid, "wxb2db021e344f1f5e")
    //获取accessToken
    var accessToken = GlobalCache.getAccessToken(appid)
    if (StringUtils.isEmpty(accessToken)) {
      val secret = EnvContextConfig.get(Constant.wechat_miniapp_secret, "045dc1788c50b00f5ad664ee7e72fedb")
      accessToken = JAccessToken.getAccessToken(appid, secret)
    }
    accessToken
  }

  def refreshAccessToken: String = {
    val appid = EnvContextConfig.get(Constant.wechat_miniapp_appid)
    val secret = EnvContextConfig.get(Constant.wechat_miniapp_secret)
    val accessToken = JAccessToken.getAccessToken(appid, secret)
    accessToken
  }
}
