package org.iartisan.wechatsdk.miniprogram.res

import scala.beans.BeanProperty

/**
  * 微信userInfo接口返回
  *
  * @author King  2019/6/19
  */
class WechatPhoneNumber {

  @BeanProperty
  var phoneNumber = ""

  @BeanProperty
  var purePhoneNumber = ""

  @BeanProperty
  var countryCode = ""

  @BeanProperty
  var watermark: Watermark = _
}

