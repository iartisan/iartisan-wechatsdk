package org.iartisan.wechatsdk.miniprogram.res

import scala.beans.BeanProperty

/**
  * 微信userInfo接口返回
  *
  * @author King  2019/6/19
  */
class WechatUserInfo {

  @BeanProperty
  var unionId = ""

  @BeanProperty
  var country = ""

  @BeanProperty
  var watermark: Watermark = _

  @BeanProperty
  var gender: Int = _

  @BeanProperty
  var province = ""

  @BeanProperty
  var city = ""

  @BeanProperty
  var avatarUrl = ""

  @BeanProperty
  var openId = ""

  @BeanProperty
  var nickName = ""

  @BeanProperty
  var language = ""
}


