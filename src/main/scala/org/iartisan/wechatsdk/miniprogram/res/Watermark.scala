package org.iartisan.wechatsdk.miniprogram.res

import scala.beans.BeanProperty

/**
  *  水印
  *
  * @author King  2019/6/24
  */
class Watermark {

  @BeanProperty
  var appid = ""

  @BeanProperty
  var timestamp = ""
}
