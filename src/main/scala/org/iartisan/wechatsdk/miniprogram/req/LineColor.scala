package org.iartisan.wechatsdk.miniprogram.req

import scala.beans.BeanProperty

/**
  *
  *
  * @author King  2019/7/1
  */
class LineColor {

  @BeanProperty
  var r = "0"

  @BeanProperty
  var g = "0"

  @BeanProperty
  var b = "0"
}