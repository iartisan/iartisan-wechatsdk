package org.iartisan.wechatsdk.miniprogram.req

import java.util.{Map => JMap}

import com.google.common.collect.Maps
import com.google.gson.annotations.SerializedName

import scala.beans.BeanProperty

/**
  * 模版消息 req
  *
  * @author King  2019-8-20
  */
class WxTemplateMsgReq {

  @SerializedName("access_token")
  @BeanProperty
  var accessToken = ""

  @BeanProperty
  var touser = null

  @SerializedName("template_id")
  @BeanProperty
  var templateId = null

  @BeanProperty
  var page = null

  @SerializedName("form_id")
  @BeanProperty
  var formId = null

  @BeanProperty
  var data: JMap[String, JMap[String, String]] = _

  @SerializedName("emphasis_keyword")
  @BeanProperty
  var emphasisKeyword = null

  def addData(name: String, value: String): Unit = {
    if (null == data) {
      data = Maps.newHashMap()
    }
    data.put(name, WxTemplateData.add(value))
  }

}

object WxTemplateData {
  def add(value: String) = {
    val params: JMap[String, String] = Maps.newHashMap()
    params.put("value", value)
    params
  }
}