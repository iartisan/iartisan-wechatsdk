package org.iartisan.wechatsdk.miniprogram.req

import scala.beans.BeanProperty

/**
  * 获取有限制的二维码
  *
  * @author King  2019/7/1
  */
class GetWxacodeReq {

  @BeanProperty
  var path = "pages/index/index"
  /**
    * 二维码的宽度，单位 px，最小 280px，最大 1280px
    */
  @BeanProperty
  var width: Int = 280
  /**
    * 自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调，默认 false
    */
  @BeanProperty
  var auto_color: Boolean = true
  /**
    * auto_color 为 false 时生效，
    * 使用 rgb 设置颜色 例如 {"r":"xxx","g":"xxx","b":"xxx"} 十进制表示
    */
  @BeanProperty
  var line_color = new LineColor
  /**
    * 是否需要透明底色，为 true 时，生成透明底色的小程序
    */
  @BeanProperty
  var is_hyaline: Boolean = false

}
