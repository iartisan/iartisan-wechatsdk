package org.iartisan.wechatsdk

import com.google.gson.GsonBuilder

/**
  * json解析类
  *
  * @author King  2019/5/29
  */
object WechatGsonBuilder {

  private val builder = new GsonBuilder


  def create = {
    builder.disableHtmlEscaping.create
  }

}


