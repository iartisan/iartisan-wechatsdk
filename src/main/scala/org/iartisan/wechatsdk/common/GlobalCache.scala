package org.iartisan.wechatsdk.common

import java.time.Duration

import com.google.common.cache.CacheBuilder
import org.iartisan.runtime.utils.StringUtils

/**
  *
  *
  * @author King  2019/5/29
  */
object GlobalCache {

  /**
    * token 保存两小时
    */
  val accessToken = CacheBuilder.newBuilder().expireAfterWrite(Duration.ofMinutes(120)).build[String, String]()


  def putAccessToken(appId: String, acccessToken: String) = {
    require(!StringUtils.isEmpty(appId), "appId不能为空")
    require(!StringUtils.isEmpty(acccessToken), "acccessToken不能为空")
    accessToken.put(appId, acccessToken)
  }

  def getAccessToken(appId: String) = {
    require(!StringUtils.isEmpty(appId), "appId不能为空")
    accessToken.getIfPresent(appId)
  }
}
