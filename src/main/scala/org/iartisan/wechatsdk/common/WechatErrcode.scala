package org.iartisan.wechatsdk.common

/**
  * 异常字典
  *
  * @author King  2019-8-20
  */
object WechatErrcode extends Enumeration {
  type WechatErrcode = Value //声明枚举对外暴露的变量类型
  val E_40001 = Value(40001)
}
