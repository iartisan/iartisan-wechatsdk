package org.iartisan.wechatsdk.common

/**
  * 字典类
  *
  * @author King  2019/5/29
  */
object Constant {

  val wechat_mp_appid = "wechat.mp.appid"

  val wechat_mp_secret = "wechat.mp.secret"

  val wechat_miniapp_appid = "wechat.miniapp.appid"

  val wechat_miniapp_secret = "wechat.miniapp.secret"

}
