package org.iartisan.wechatsdk.common.res

import java.lang.reflect.Type

import com.google.gson.annotations.JsonAdapter
import com.google.gson.{JsonDeserializationContext, JsonDeserializer, JsonElement}

import scala.beans.BeanProperty

/**
  * access_token 返回码
  *
  * @author King  2019/5/29
  */
@JsonAdapter(classOf[WechatAccessTokenAdapter])
class WechatAccessTokenRes {

  @BeanProperty
  var accessToken: String = _


  @BeanProperty
  var expiresIn: Int = _

}

class WechatAccessTokenAdapter extends JsonDeserializer[WechatAccessTokenRes] {
  override def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): WechatAccessTokenRes = {
    val accessToken = new WechatAccessTokenRes
    val jsonObj = json.getAsJsonObject
    accessToken.setAccessToken(jsonObj.get("access_token").getAsString)
    accessToken.setExpiresIn(jsonObj.get("expires_in").getAsInt)
    accessToken
  }
}
