package org.iartisan.wechatsdk.common.res

import scala.beans.BeanProperty

/**
  * 微信错误时返回的包
  *
  * @author King  2019/5/29
  */
class WechatResError {

  @BeanProperty
  var errcode: Int = _

  @BeanProperty
  var errmsg: String = _
}
