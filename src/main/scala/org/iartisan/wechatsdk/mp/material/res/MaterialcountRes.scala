package org.iartisan.wechatsdk.mp.material.res

import java.lang.reflect.Type

import com.google.gson.annotations.JsonAdapter
import com.google.gson.{JsonDeserializationContext, JsonDeserializer, JsonElement}

import scala.beans.BeanProperty

/**
  * 素材总数返回
  *
  * @author King  2019/5/29
  */

@JsonAdapter(classOf[MaterialcountResAdapter])
class MaterialcountRes {

  @BeanProperty
  var voiceCount: Int = _

  @BeanProperty
  var videoCount: Int = _

  @BeanProperty
  var imageCount: Int = _

  @BeanProperty
  var newsCount: Int = _

}

class MaterialcountResAdapter extends JsonDeserializer[MaterialcountRes] {
  override def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): MaterialcountRes = {
    val res = new MaterialcountRes
    val jsonObj = json.getAsJsonObject
    if (null != jsonObj) {
      res.setImageCount(if (jsonObj.get("image_count") != null) jsonObj.get("image_count").getAsInt else 0)
      res.setVideoCount(if (jsonObj.get("video_count") != null) jsonObj.get("video_count").getAsInt else 0)
      res.setVoiceCount(if (jsonObj.get("voice_count") != null) jsonObj.get("voice_count").getAsInt else 0)
      res.setNewsCount(if (jsonObj.get("news_count") != null) jsonObj.get("news_count").getAsInt else 0)
    }
    res
  }
}
