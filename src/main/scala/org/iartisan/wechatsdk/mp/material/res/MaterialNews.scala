package org.iartisan.wechatsdk.mp.material.res

import java.lang.reflect.Type
import java.util.Date

import com.google.common.collect.Lists
import com.google.gson.annotations.JsonAdapter
import com.google.gson.{JsonDeserializationContext, JsonDeserializer, JsonElement}
import org.iartisan.wechatsdk.WechatGsonBuilder
import org.apache.commons.lang3.BooleanUtils

import scala.beans.BeanProperty

/**
  * 素材新闻
  *
  * @author King  2019/5/29
  */
@JsonAdapter(classOf[MaterialNewsAdapter])
class MaterialNews {

  @BeanProperty
  var createTime: Date = _

  @BeanProperty
  var updateTime: Date = _

  @BeanProperty
  var articles: java.util.List[MaterialNewsArticle] = _

}

@JsonAdapter(classOf[MaterialNewsArticleAdapter])
class MaterialNewsArticle {
  /**
    * (必填) 图文消息缩略图的media_id，可以在基础支持-上传多媒体文件接口中获得.
    */
  @BeanProperty var thumbMediaId: String = _
  /**
    * 图文消息的封面url.
    */
  @BeanProperty var thumbUrl: String = _
  /**
    * 图文消息的作者.
    */
  @BeanProperty var author: String = _
  /**
    * (必填) 图文消息的标题.
    */
  @BeanProperty var title: String = _
  /**
    * 在图文消息页面点击“阅读原文”后的页面链接.
    */
  @BeanProperty var contentSourceUrl: String = _
  /**
    * (必填) 图文消息页面的内容，支持HTML标签.
    */
  @BeanProperty var content: String = _
  /**
    * 图文消息的描述.
    */
  @BeanProperty var digest: String = _
  /**
    * 是否显示封面，true为显示，false为不显示.
    */
  @BeanProperty var showCoverPic = false

  /**
    * 点击图文消息跳转链接.
    */
  @BeanProperty var url: String = _
}

class MaterialNewsAdapter extends JsonDeserializer[MaterialNews] {
  override def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): MaterialNews = {
    val res = new MaterialNews
    val jsonObj = json.getAsJsonObject
    if (jsonObj.get("news_item") != null && !jsonObj.get("news_item").isJsonNull) {
      val item = jsonObj.getAsJsonArray("news_item")
      val dataList: java.util.List[MaterialNewsArticle] = Lists.newArrayList()
      for (i <- 0 until item.size()) {
        val article = WechatGsonBuilder.create.fromJson(item.get(i), classOf[MaterialNewsArticle])
        dataList.add(article)
      }
      res.setArticles(dataList)
    }

    if (jsonObj.get("create_time") != null && !jsonObj.get("create_time").isJsonNull) {
      val createTime = new Date(jsonObj.get("create_time").getAsLong * 1000)
      res.setCreateTime(createTime)
    }

    if (jsonObj.get("update_time") != null && !jsonObj.get("update_time").isJsonNull) {
      val updateTime = new Date(jsonObj.get("update_time").getAsLong * 1000)
      res.setUpdateTime(updateTime)
    }
    res
  }
}

class MaterialNewsArticleAdapter extends JsonDeserializer[MaterialNewsArticle] {
  override def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): MaterialNewsArticle = {
    val jsonObj = json.getAsJsonObject
    val res = new MaterialNewsArticle

    val title = jsonObj.get("title")
    if (title != null && !title.isJsonNull) {
      res.setTitle(title.getAsString)
    }
    val content = jsonObj.get("content")
    if (content != null && !content.isJsonNull) {
      res.setContent(content.getAsString)
    }
    val contentSourceUrl = jsonObj.get("content_source_url")
    if (contentSourceUrl != null && !contentSourceUrl.isJsonNull) {
      res.setContentSourceUrl(contentSourceUrl.getAsString)
    }
    val author = jsonObj.get("author")
    if (author != null && !author.isJsonNull) {
      res.setAuthor(author.getAsString)
    }
    val digest = jsonObj.get("digest")
    if (digest != null && !digest.isJsonNull) {
      res.setDigest(digest.getAsString)
    }
    val thumbMediaId = jsonObj.get("thumb_media_id")
    if (thumbMediaId != null && !thumbMediaId.isJsonNull) {
      res.setThumbMediaId(thumbMediaId.getAsString)
    }
    val thumbUrl = jsonObj.get("thumb_url")
    if (thumbUrl != null && !thumbUrl.isJsonNull) {
      res.setThumbUrl(thumbUrl.getAsString)
    }
    val showCoverPic = jsonObj.get("show_cover_pic")
    if (showCoverPic != null && !showCoverPic.isJsonNull) {
      res.setShowCoverPic(BooleanUtils.toBoolean(showCoverPic.getAsInt))
    }
    val url = jsonObj.get("url")
    if (url != null && !url.isJsonNull) {
      res.setUrl(url.getAsString)
    }
    res
  }
}
