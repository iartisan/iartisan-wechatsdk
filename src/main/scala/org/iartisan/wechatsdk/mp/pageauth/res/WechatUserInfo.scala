package org.iartisan.wechatsdk.mp.pageauth.res

import scala.beans.BeanProperty

/**
 * 微信用户信息
 *
 * @author King
 * @date 2020-02-09
 */
class WechatUserInfo {

  @BeanProperty
  var openid: String = _

  @BeanProperty
  var nickname: String = _

  @BeanProperty
  var sex: String = _

  @BeanProperty
  var city: String = _

  @BeanProperty
  var country: String = _

  @BeanProperty
  var headimgurl: String = _

  @BeanProperty
  var privilege: Array[String] = _

  @BeanProperty
  var unionid: String = _

}
