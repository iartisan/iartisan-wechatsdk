package org.iartisan.wechatsdk.mp.pageauth.res

import com.google.gson.annotations.SerializedName

import scala.beans.BeanProperty

/**
 * 网页授权access_token
 *
 * @author King
 * @date 2020-02-09
 */
class PageAuthAccessToken {

  @SerializedName("access_token")
  @BeanProperty
  var accessToken: String = _

  @SerializedName("expires_in")
  @BeanProperty
  var expiresIn: Int = _

  @SerializedName("refresh_token")
  @BeanProperty
  var refreshToken: String = _

  @BeanProperty
  var openid: String = _

  @BeanProperty
  var scope: String = _

}
